package com.garmin.plm.duncan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringRancherPipelineApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringRancherPipelineApplication.class, args);
    }

}
