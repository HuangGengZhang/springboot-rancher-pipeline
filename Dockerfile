FROM maven:3.6.3-jdk-11

RUN mkdir -p /usr/src/myapp

COPY . /usr/src/myapp/

WORKDIR /usr/src/myapp

RUN mvn clean package

CMD ["java", "-jar", "spring-rancher-pipeline-0.0.1.jar"]
